#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

pushd src

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# Required when building against boost 1.74,
# as boost::container_hash uses std::unary_function, 
# which has been removed in C++17
export CPPFLAGS=-DBOOST_NO_CXX98_FUNCTION_BASE=1

make
make install
popd
