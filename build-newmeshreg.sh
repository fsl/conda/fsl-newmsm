#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

pushd libraries/msm-newmeshreg/src

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# In newmeshreg 0.3.1, the Fusion and FastPD files
# have been separated out and turned into header files.
# But they are required by newmsm as well, so we need
# them in the source dir so they will be installed
# into $PREFIX/include/
cp ../include/Fusion/* .
cp ../include/FastPD/* .

# Required when building against boost 1.74,
# as boost::container_hash uses std::unary_function,
# which has been removed in C++17
export CPPFLAGS="-DBOOST_NO_CXX98_FUNCTION_BASE=1"

make
make install
popd
